<?php

/*
|--------------------------------------------------------------------------
| Available entities VND routes
|--------------------------------------------------------------------------
|
| List of available routes to manage entities.
| It is based on the media type application/vnd.api+json (https://jsonapi.org/format/).
| An entity at "true" will use group default setting.
| An entity at "false" will neither be displayed nor manageable. We can also simply omit the entity.
| An entity can ovewrite group setting by applying an array with the keys we want to overwrite (i.e. 'my_entity' => ['method' => ['get', 'post', 'patch']])
|
| Note that the route retrictions should not be used for rights permissions, tis can be done by "roles" (if any) in API resources, and/or the API rights (if any).
|
| Regardless the entity is soft or hard deleted, we list all routes by default, any attempt to
|
*/

return [

    // API Resources CMS
    'api_res_cms' => [ // Group name
        'controller' => 'GatewayController@proxyCMS', // Default Controller method to reach
        'api' => env('API_RES_CMS'), // Default API to target
        'ip' => env('API_RES_CMS_IP'), // Default IP to target
        'port' => env('API_RES_CMS_PORT', 80), // Default PORT to target (80 can be used to speed up because it's an internal communication)
        'client_prefix' => 'cms', // Default Client URI prefix
        'server_prefix' => 'brunoocto/cmswl', // Default Server URI prefix
        'middleware' => ['auth:api'], // Default Middlewares to use
        'method' =>  ['get', 'post', 'patch', 'delete', 'put'], // Default Methods to be used
        'entities' => [
            'packages' => true,
            'users' => true,
            'profiles' => true,
            'series' => true,
            'roles' => true,
            'settings' => true,
            'seasons' => true,
            'thematics' => true,
            'typologies' => true,
            'notifications' => true,
            'linears' => true,
            'non_linears' => true,
            'comments' => true,
            'files' => true,
            'offers' => true,
            'subscriptions' => true,
            'devices' => true,
            'device_types' => true,
        ]
    ],


];
