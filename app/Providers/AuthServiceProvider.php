<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Providers\LinckoUserProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        /**
         * Only for the routes that needs the middleware "api:auth" to check the Bearer token,
         * we use the driver "lincko" which returns an instance of LinckoUserProvider.
         * The provider will then check the Bearer token with the API Res Auth and simply get the user ID from it.
         */
        \Auth::provider('lincko', function ($app, array $config) {
            return new LinckoUserProvider($app['hash'], $config['model']);
        });
    }
}
