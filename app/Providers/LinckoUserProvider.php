<?php

namespace App\Providers;

use Illuminate\Auth\EloquentUserProvider;
use GuzzleHttp\Client as Guzzle;

class LinckoUserProvider extends EloquentUserProvider
{

    /**
     * Retrieve a user by the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        // Get Authroization string from header
        $bearer = request()->bearerToken();
        if (empty($bearer)) {
            return \LinckoJson::error(401, 'Missing Authorization information in the header.');
        }

        // TODO (cache) => Consult Redis if the bearer exists

        // Get Operator string from header
        $operator = request()->header('Operator');
        if (!preg_match('/^\w+$/', $operator)) {
            return \LinckoJson::error(401, 'Missing Operator information in the header.');
        }

        // Make sure that the URL finish by a "/"
        $api = env('API_RES_AUTH');
        if (substr($api, -1) != '/') {
            $api .= '/';
        }
        // Parse URL
        $url = parse_url($api);

        $url['url'] = env('API_RES_AUTH');

        $url['ip'] = env('API_RES_AUTH_IP');

        // Get Port from .env
        $port = env('API_RES_AUTH_PORT');
        // If not set, get it from URL
        if (empty($port)) {
            if (isset($url['port'])) {
                // Get port from URL if any (https://example.com:8888/test)
                $port = $url['port'];
            } elseif (isset($url['scheme']) && strtolower($url['scheme']) == 'https') {
                // 443 if https
                $port = 443;
            } else {
                // Default is 80 (http)
                $port = 80;
            }
        }
        $url['port'] = $port;

        // Get User ID from API Res Auth
        $guzzle = new Guzzle([
            'curl' => [
                CURLOPT_RESOLVE => [ $url['host'].':'.$url['port'].':'.$url['ip'] ]
            ],
            'headers' => [
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json',
                'Authorization' => 'Bearer '.$bearer,
                'Operator'      => $operator,
            ],
        ]);

        // Send the request
        $response = $guzzle->get($url['url'].'api/user');

        // Get the response body
        $body = json_decode($response->getBody()->getContents());
        
        if (
            $response->getStatusCode() == 200
            && isset($body->success) && $body->success
            && isset($body->user)
        ) {
            // Create Local User model
            $user = new $this->model;
            // Attach the ID found
            $user->id = $body->user->id;
            return $user;
        }
        
        return null;
    }
}
