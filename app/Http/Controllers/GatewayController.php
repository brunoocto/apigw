<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client as Guzzle;
use Nahid\JsonQ\Jsonq;

//use GuzzleHttp\Psr7\Request as Guzzle;

class GatewayController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Api Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for consulting resources.
    |
    */

    /**
     * Response body
     */
    protected $content = null;

    /**
     * Does consult the API Res Rights to check if the user is denied to do any CRUD operation
     *
     * @param \stdClass $model Object model to target {type, id}
     * @param integer $crud_level CRUD request information (1:Read, 2:Create, 3:Update, 4:Delete)
     * @return bool
     */
    protected function checkRights(\stdClass $model, $crud_level = 1) : bool
    {
        // Check that for Update and Delete, the ID is provided
        if (in_array($crud_level, [3, 4]) && is_null($model->id)) {
            return false;
        }

        // TODO => Contact the API Res Auth and return true if ok

        // By Default we are permissive
        return true;
    }

    /**
     * Return an array with URL information to connect to the API Resource Authentication
     *
     * @return Array
     */
    protected function getAuthURL()
    {
        // Make sure that the URL finish by a "/"
        $api = env('API_RES_AUTH');
        if (substr($api, -1) != '/') {
            $api .= '/';
        }
        // Parse URL
        $url = parse_url($api);

        $url['url'] = env('API_RES_AUTH');

        $url['ip'] = env('API_RES_AUTH_IP');

        // Get Port from .env
        $port = env('API_RES_AUTH_PORT');
        // If not set, get it from URL
        if (empty($port)) {
            if (isset($url['port'])) {
                // Get port from URL if any (https://example.com:8888/test)
                $port = $url['port'];
            } elseif (isset($url['scheme']) && strtolower($url['scheme']) == 'https') {
                // 443 if https
                $port = 443;
            } else {
                // Default is 80 (http)
                $port = 80;
            }
        }
        $url['port'] = $port;

        return $url;
    }

    /**
     * Filter the content according to rules
     * 
     * @param  Illuminate\Http\Request  $request
     * @return void
     */
    protected function filterContent(Request $request)
    {
        // TODO => Filter the content according to configuration rules specific to the client used
        // TODO => We may use yield to reduce memory usage
        // Filter $this->content;

        // if (is_string($this->content)) {
        //     $this->content = json_decode($this->content);
        // }
    }

    /**
     * Format the content according to rules
     *
     * @param  Illuminate\Http\Request  $request
     * @return void
     */
    protected function formatContent(Request $request)
    {
        // TODO => Format the content according to configuration rules specific to the client used
        // TODO => We may use yield to reduce memory usage
        // Format $this->content;

        // if (is_string($this->content)) {
        //     $this->content = json_decode($this->content);
        // }
    }

    /**
     * Request API Resources CMS
     *
     * @param  Illuminate\Http\Request  $request
     * @return GuzzleHttp\Psr7\Response
     */
    protected function requestApiCMS(Request $request)
    {
        /**
         * We first convert an URI like "/cms/packages" into "packages" (without first slash and prefix).
         * It helps to call the API by its entity only.
         */

        // Get URI and remove any "/" at the beginning and the end
        $uri = $request->path();
        $uri = preg_replace('/^\/?(.*?)\/?$/', '$1', $request->path());

        $prefix = $request->route()->getPrefix();

        if (!empty($prefix)) {
            // If a prefix is set, we remove it from the URI
            // We remove any "/" at the beginning and the end
            $prefix = preg_replace('/^\/?(.*?)\/?$/', '$1', $prefix);

            // Remove the prefix
            $uri = preg_replace('/^'.preg_quote($prefix, '/').'(.*)$/', '$1', $uri);

            // We remove again slashes from URI
            $uri = preg_replace('/^\/?(.*?)\/?$/', '$1', $uri);
        }


        // NOTE: I don't use this part because I delete the feature to replace the name swap (i.e.: tasks => todos)
        // // The App route (with the displayed name)
        // $type = $request->route()->getAction('type');
        // // The Resource route (with the model real name)
        // $this->entity = $request->route()->getAction('entity');
        // // If the name is the same, we don't need to swap it
        // if ($this->entity != $type) {
        //     // We swap only the name at the beginning of the string
        //     $uri = preg_replace('/^'.$type.'(\/|$)/', $this->entity.'$1', $uri);
        // }
        // // In case of relationship, we convert also its name
        // if (preg_match('/^'.$this->entity.'\/\d+\/(?:relationships\/)?(\w+)\/?$/', $uri, $matches)) {
        //     // Catch the relation name
        //     $relation = $matches[1];
        //     // Get the group name of where we are working
        //     $group = $request->route()->getAction('group');
        //     // Get the entities list
        //     $entities = config('apivnd.'.$group.'.entities');
        //     // We check if the relation name exists in the configuration and the name is swapped
        //     if (is_array($entities) && isset($entities[$relation]) && isset($entities[$relation]['entity'])) {
        //         // We swap the relation name in the URI
        //         $uri = preg_replace('/^('.$this->entity.'\/\d+\/(?:relationships\/)?)\w+\/?$/', '$1'.$entities[$relation]['entity'], $uri);
        //     }
        // }

        /**
         * We proxy the request to the API
         */

        // Get URL detail
        $url = $request->route()->getAction('url');

        // Get method
        $method = strtolower($request->getMethod());

        // Get the CRUD level requested according to the HTTP method
        switch ($method) {
            case 'post':
                $crud_level = 2; // Create
                break;
            case 'patch':
                $crud_level = 3; // Update
                break;
            case 'delete':
                $crud_level = 4; // Delete
                break;
            default:
                // GET and PUT
                $crud_level = 1; // Default as Read
        }

        $model = new \stdClass;
        // Set Model type
        $model->type = $request->route()->getAction('entity');
        $model->id = null;

        // Set Model ID if any specified
        $parameters = $request->route()->parameters();
        
        if (isset($parameters['id'])) {
            $model->id = $parameters['id'];
        }

        // We check if the user is allow to do the operation
        if (!$this->checkRights($model, $crud_level)) {
            return \LinckoJson::error(403, 'You do not have the permission to access this resource.');
        }

        // Get Operator string from header
        $operator = request()->header('Operator');
        if (!preg_match('/^\w+$/', $operator)) {
            return \LinckoJson::error(401, 'Missing Operator information in the header.');
        }

        $guzzle = new Guzzle([
            'curl' => [
                CURLOPT_RESOLVE => [ $url['host'].':'.$url['port'].':'.$url['ip'] ]
            ],
            'headers' => [
                'Content-Type'  => 'application/vnd.api+json',
                'Accept'        => 'application/vnd.api+json',
                'Custom-Method' => strtoupper($method),
                'User-Id'       => \Auth::id(), // Authenticated User ID
                'Operator'      => $operator,
            ],
            'body' => $request->getContent(),
        ]);

        // Convert the method for security reason (webdav)
        if (in_array($method, ['patch', 'put', 'delete'])) {
            $method = 'post';
        }

        // Get parameters if any
        $parameters = $this->getParametersAsString($parameters);
        if (!empty($parameters)) {
            // We add question mark if any parameters passed
            $parameters = '?'.$parameters;
        }
        
        // Send the request
        $response = $guzzle->$method($url['url'].$uri.$parameters);

        // Return the response
        return $response;
    }

    /**
     * Request API Resources Auth for Login
     * Here is a document to understand OAuth 2.0 workflow:
     * https://auth0.com/docs/api-auth/which-oauth-flow-to-use
     *
     * @param  Illuminate\Http\Request  $request
     * @return Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        // Validate the object received
        $request->validate([
            'email' => 'required|string|min:3|email:rfc,dns',
            'password' => 'required|string|min:3',
        ]);
        // Construct the body to send
        $body = [
            'grant_type' => 'password',
            'client_id' => env('API_RES_AUTH_CLIENT_ID'),
            'client_secret' => env('API_RES_AUTH_CLIENT_SECRET'),
            'username' => $request->input('email'), // We identify by unique email
            'password' => $request->input('password'),
        ];

        // Stringify the body
        $body = json_encode($body, true);

        // Construct URL to connect to the API Auth
        $url = $this->getAuthURL();

        $guzzle = new Guzzle([
            'curl' => [
                CURLOPT_RESOLVE => [ $url['host'].':'.$url['port'].':'.$url['ip'] ]
            ],
            'headers' => [
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json',
                'Custom-Method' => 'POST',
            ],
            'body' => $body,
        ]);

        // Send the request
        $response = $guzzle->post($url['url'].'oauth/token');

        // Get the response body
        $body = $response->getBody()->getContents();
        // Return the response
        return response($body, $response->getStatusCode())
            ->withHeaders($response->getHeaders());
    }

    /**
     * Request API Resources Auth for Refresh
     * Here is a document to understand OAuth 2.0 workflow:
     * https://auth0.com/docs/api-auth/which-oauth-flow-to-use
     *
     * @param  Illuminate\Http\Request  $request
     * @return Illuminate\Http\Response
     */
    public function refresh(Request $request)
    {
        // Validate the object received
        $request->validate([
            'refresh_token' => 'required|string|regex:/^\w+$/i',
        ]);
        
        // Construct the body to send
        $body = [
            'grant_type' => 'refresh_token',
            'client_id' => env('API_RES_AUTH_CLIENT_ID'),
            'client_secret' => env('API_RES_AUTH_CLIENT_SECRET'),
            'refresh_token' => $request->input('refresh_token'),
        ];

        // Stringify the body
        $body = json_encode($body, true);

        // Construct URL to connect to the API Auth
        $url = $this->getAuthURL();

        $guzzle = new Guzzle([
            'curl' => [
                CURLOPT_RESOLVE => [ $url['host'].':'.$url['port'].':'.$url['ip'] ]
            ],
            'headers' => [
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json',
                'Custom-Method' => 'POST',
            ],
            'body' => $body,
        ]);

        // Send the request
        $response = $guzzle->post($url['url'].'oauth/token');

        // Get the response body
        $body = $response->getBody()->getContents();
        // Return the response
        return response($body, $response->getStatusCode())
            ->withHeaders($response->getHeaders());
    }

    /**
     * Request API Resources Auth for Logout
     * Here is a document to understand OAuth 2.0 workflow:
     * https://auth0.com/docs/api-auth/which-oauth-flow-to-use
     *
     * @param  Illuminate\Http\Request  $request
     * @return Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        // Construct URL to connect to the API Auth
        $url = $this->getAuthURL();

        $guzzle = new Guzzle([
            'curl' => [
                CURLOPT_RESOLVE => [ $url['host'].':'.$url['port'].':'.$url['ip'] ]
            ],
            'headers' => [
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json',
                'Custom-Method' => 'POST',
                'Authorization' => $request->header('authorization'),
            ],
        ]);
        
        // Send the request
        $response = $guzzle->post($url['url'].'api/user/logout');
        
        // Get the response body
        $body = $response->getBody()->getContents();
        // Return the response
        return response($body, $response->getStatusCode())
            ->withHeaders($response->getHeaders());
    }

    /**
     * List all entities available and their CRUD allowed
     *
     * @return Illuminate\Http\Response
     */
    public function entities()
    {
        // Get VND configuration
        $groups = config('apivnd');
        $data = [];
        // Check all groups
        foreach($groups as $group){
            $method = $group['method'];
            $url = $group['client_prefix'];
            if(isset($group['entities'])){
                // Check all entities
                foreach($group['entities'] as $name => $entity){
                    if(!$entity){
                        // Skip if the entity is set at false
                        continue;
                    }
                    // Get the route prefix
                    $client_prefix = isset($entity['client_prefix']) ? $entity['client_prefix'] : $group['client_prefix'];
                    // Get the methods allowed
                    $method = isset($entity['method']) ? $entity['method'] : $group['method'];
                    if(!isset($data[$client_prefix])){
                        $data[$client_prefix] = [];
                    }
                    // Save
                    $data[$client_prefix][$name] = $method;
                }
            }
        }
        
        // Return the response
        return \LinckoJson::send($data);
    }

    /**
     * Request API Resources Auth
     * Here is a document to understand OAuth 2.0 workflow:
     * https://auth0.com/docs/api-auth/which-oauth-flow-to-use
     *
     * @param  Illuminate\Http\Request  $request
     * @return Illuminate\Http\Response
     */
    public function proxyOAuth(Request $request)
    {
        // Construct URL to connect to the API Auth
        $url = $this->getAuthURL();

        // Get URI
        $uri = $request->path();

        $prefix = $request->route()->getPrefix();

        if (!empty($prefix)) {
            // If a prefix is set, we remove it from the URI
            // We remove any "/" at the beginning and the end
            $prefix = preg_replace('/^\/?(.*?)\/?$/', '$1', $prefix);

            // Remove the prefix
            $uri = preg_replace('/^'.preg_quote($prefix, '/').'(.*)$/', '$1', $uri);

            // We remove again slashes from URI
            $uri = preg_replace('/^\/?(.*?)\/?$/', '$1', $uri);
        }

        /**
         * We proxy the request to the API
         */

        // Get method
        $method = strtolower($request->getMethod());

        $guzzle = new Guzzle([
            'curl' => [
                CURLOPT_RESOLVE => [ $url['host'].':'.$url['port'].':'.$url['ip'] ]
            ],
            'headers' => [
                'Content-Type'  => 'application/json',
                'Accept'        => 'application/json',
                'Custom-Method' => strtoupper($method),
            ],
            'body' => $request->getContent(),
        ]);

        // Convert the method for security reason (webdav)
        if (in_array($method, ['patch', 'put', 'delete'])) {
            $method = 'post';
        }

        // Get parameters if any
        $parameters = $request->getQueryString();
        if (!empty($parameters)) {
            // We add question mark if any parameters passed
            $parameters = '?'.$parameters;
        }

        // Send the request
        $response = $guzzle->$method($url['url'].$uri.$parameters);

        // Get the response body
        $body = $response->getBody()->getContents();
        // Return the response
        return response($body, $response->getStatusCode())
            ->withHeaders($response->getHeaders());
    }

    /**
     * Request API Resources CMS
     *
     * @param  Illuminate\Http\Request  $request
     * @return Illuminate\Http\Response
     */
    public function proxyCMS(Request $request)
    {
        // Get the response from API Resource
        $response = $this->requestApiCMS($request);
        // Get the content
        $this->content = $response->getBody()->getContents();
        
        // Filter the content (as an array)
        $this->filterContent($request);
        // Format the content (as an array)
        $this->formatContent($request);
        
        // Encode the Content
        if (!is_string($this->content)) {
            $this->content = json_encode($this->content);
        }

        return response($this->content, $response->getStatusCode())
            ->withHeaders($response->getHeaders());
    }
}
