<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client as Guzzle;
use App\Http\Controllers\GatewayController;
use Illuminate\Http\UploadedFile;

//use GuzzleHttp\Psr7\Request as Guzzle;

class UploadController extends GatewayController
{
    public function upload(Request $request)
    {
        $moveSucces = '';
        foreach($request->allFiles() as $file){
            $ext  = $file->getClientOriginalExtension();
            $moveSucces .= $file->move('/var/www/assets/', $file->getClientOriginalName());
        }
        return $moveSucces;
    }
}
