<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group.
|
*/


/**
 * Map OAuth resources routes
 */

// Credential operations
\Route::post('auth/login', 'GatewayController@login')
    ->name('auth.login');

\Route::post('auth/refresh', 'GatewayController@refresh')
    ->name('auth.login');

\Route::post('auth/logout', 'GatewayController@logout')
    ->middleware('auth:api')
    ->name('auth.logout');

// Non VND Routes
\Route::group(['as' => 'nonVND','middleware' => ['auth:api',],'prefix' => 'cms'], function () {
    \Route::get('series/favorites', 'SeriesController@proxyCMS');
});

// Files
\Route::post('/files/upload', 'UploadController@upload')
    ->middleware('auth:api')
    ->name('files.upload');

/**
 * Map VND resources routes
 */

\Route::get('/entities', 'GatewayController@entities')
    ->middleware('auth:api')
    ->name('entities');

$apivnd = config('apivnd');

foreach ($apivnd as $group_key => $group) {
    // Default API called
    $controller_default = $group['controller'];
    $api_default = $group['api'];
    $ip_default = $group['ip'];
    $port_default = $group['port'];

    // (optional) Default Client URI prefix
    $client_prefix_default = isset($group['client_prefix']) ? $group['client_prefix'] : '';

    // (optional) Default Server URI prefix
    $server_prefix_default = isset($group['server_prefix']) ? $group['server_prefix'] : '';
    
    // (optional) Default Middlewares to use
    $middleware_default = isset($group['middleware']) ? $group['middleware'] : [];

    // (optional) Default Middlewares to use
    $method_default = isset($group['method']) ? $group['method'] : ['get'];

    /**
     * Map Vmodel routes
     *
     * GET    /{type}
     * GET    /{type}/{id}
     * GET    /{type}/{id}/relationships/{relationship_type}
     * GET    /{type}/{id}/{relationship_type}
     * POST   /{type}
     * PATCH  /{type}/{id}
     * DELETE /{type}/{id}
     * PUT    /{type}/{id}/restore
     * PUT    /{type}/{id}/force_delete
     *
     */

    foreach ($group['entities'] as $key => $entity) {
        // Skip the entity if it is set to false
        if (!$entity) {
            continue;
        }
        // Set value for the entity itself, overwrite group value if the key is redefined in the entity.

        $controller = isset($entity['controller']) ? $entity['controller'] : $controller_default;
        $client_prefix = isset($entity['client_prefix']) ? $entity['client_prefix'] : $client_prefix_default;
        $server_prefix = isset($entity['server_prefix']) ? $entity['server_prefix'] : $server_prefix_default;
        // Make sure that the Server prefix finish by a "/"
        if (!empty($server_prefix) && substr($server_prefix, -1) != '/') {
            $server_prefix .= '/';
        }
        $middleware = isset($entity['middleware']) ? $entity['middleware'] : $middleware_default;
        $method = isset($entity['method']) ? $entity['method'] : $method_default;
        
        // Get the name of Backend entity from the file of route config
        $entity    = isset($entity['entity']) ? $entity['entity'] : $key;

        // Make sure that all method names are lower case
        $method = array_map('strtolower', $method);

        $api = isset($entity['api']) ? $entity['api'] : $api_default;
        // Make sure that the URL finish by a "/"
        if (substr($api, -1) != '/') {
            $api .= '/';
        }

        // Parse URL
        $url = parse_url($api);

        // Set full URL (it must be an URL without any GET parameter)
        $url['url'] = $api.$server_prefix;
        
        // Set IP if any
        $url['ip'] = isset($entity['ip']) ? $entity['ip'] : $ip_default;

        // Get Port from .env
        $port = isset($entity['port']) ? $entity['port'] : $port_default;
        // If not set, get it from URL
        if (empty($port)) {
            if (isset($url['port'])) {
                // Get port from URL if any (https://example.com:8888/test)
                $port = $url['port'];
            } elseif (isset($url['scheme']) && strtolower($url['scheme']) == 'https') {
                // 443 if https
                $port = 443;
            } else {
                // Default is 80 (http)
                $port = 80;
            }
        }
        $url['port'] = $port;

        \Route::group([
            'middleware' => $middleware,
            'prefix' => $client_prefix,
        ], function () use ($controller, $method, $group_key, $key, $entity, $url) {

            // GET method
            if (in_array('get', $method)) {
                \Route::get($key.'/{id?}/{param_one?}/{param_two?}', [
                    'as' => $group_key.'.'.$key,
                    'uses' => $controller,
                    // Send to the controler the values of the API to use
                    'url' => $url,
                    'type' => $key,
                    'entity' => $entity,
                     // Group name used to scan all entities in the middleware
                    'group' => $group_key,
                    'where' => [
                        'id' => '\d+',
                        'param_one' => '\w+',
                        'param_two' => '\w+',
                    ]
                ]);
            }

            // POST method
            if (in_array('post', $method, $entity)) {
                \Route::post($key, [
                    'as' => $group_key.'.'.$key,
                    'uses' => $controller,
                    'url' => $url,
                    'type' => $key,
                    'entity' => $entity,
                    // Group name used to scan all entities in the middleware
                    'group' => $group_key,
                ]);
            }

            // PATCH method
            if (in_array('patch', $method, $entity)) {
                \Route::patch($key.'/{id?}', [
                    'as' => $group_key.'.'.$key,
                    'uses' => $controller,
                    'url' => $url,
                    'type' => $key,
                    'entity' => $entity,
                    // Group name used to scan all entities in the middleware
                    'group' => $group_key,
                    'where' => [
                        'id' => '\d+',
                    ]
                ]);
            }

            // DELETE method
            if (in_array('delete', $method, $entity)) {
                \Route::delete($key.'/{id?}', [
                    'as' => $group_key.'.'.$key,
                    'uses' => $controller,
                    'url' => $url,
                    'type' => $key,
                    'entity' => $entity,
                    // Group name used to scan all entities in the middleware
                    'group' => $group_key,
                    'where' => [
                        'id' => '\d+',
                    ]
                ]);
            }

            // PUT method
            if (in_array('put', $method)) {
                \Route::put($key.'/{id?}/{param_one?}', [
                    'as' => $group_key.'.'.$key,
                    'uses' => $controller,
                    'url' => $url,
                    'type' => $key,
                    'where' => [
                        'id' => '\d+',
                        'param_one' => 'restore|force_delete',
                    ]
                ]);
            }
        });
    }
}
