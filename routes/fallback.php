<?php

/*
|--------------------------------------------------------------------------
| Fallback Route
|--------------------------------------------------------------------------
|
| Here is where the user arrive if the page is not accessible
|
*/

Route::get('404', function () {
    return 'Were you looking for a 404 ? Hmm, looks like there is nothing else to see here...';
})->name('404');

Route::fallback(function () {
    return redirect()->route('404');
});
